﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace APIRESTSQLserver
{
    public class StorageSQL
    {
        public List<Clientes> ListaDeClientes;
        public List<Clientes> Consulta()
        {
            var dt = new DataTable();
            string cadena = "";
            var connect = new SqlConnection(cadena);
            var cmd = new SqlCommand($"select * from Data", connect);

            try
            {
                ListaDeClientes = new List<Clientes>();
                connect.Open();
                var da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                connect.Close();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Clientes clientes = new Clientes()
                    {
                        id = int.Parse(dt.Rows[i]["id"].ToString()),
                        name = dt.Rows[i]["name"].ToString(),
                        address = dt.Rows[i]["address"].ToString(),
                        email = dt.Rows[i]["email"].ToString(),
                        age = int.Parse(dt.Rows[i]["age"].ToString()),
                        balance = double.Parse(dt.Rows[i]["balance"].ToString()),
                        latitude = dt.Rows[i]["latitude"].ToString(),
                        longitude = dt.Rows[i]["longitude"].ToString(),
                        image = dt.Rows[i]["image"].ToString(),
                        background = dt.Rows[i]["background"].ToString()
                    };

                    ListaDeClientes.Add(clientes);
                }

                return ListaDeClientes;
            }
            catch (System.Exception)
            {
                connect.Close();
                return ListaDeClientes;
            }

        }

        public bool save(string name, string address, string email, int age, double balance, string latitude, string longitude, string image, string background)
        {
            string cadena = "";
            string command = $"insert into Data values('{name}', '{address}', '{email}', {age}, {balance}, '{latitude}', '{longitude}', '{image}', '{background}')";
            var connect = new SqlConnection(cadena);
            var query = new SqlCommand(command, connect);

            try
            {
                connect.Open();
                query.ExecuteNonQuery();
                connect.Close();
                return true;
            }
            catch (System.Exception)
            {
                connect.Close();
                return false;
            }
        }
    }
}
