﻿using System.Collections.Generic;

namespace APIRESTSQLserver
{
    public class Clientes
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string email { get; set; }
        public int age { get; set; }
        public double balance { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string image { get; set; }
        public string background { get; set; }
    }

    public class ClientesResponse
    {
        public bool status { get; set; }
        public List<Clientes> data { get; set; }
        public string message { get; set; }
    }
}
