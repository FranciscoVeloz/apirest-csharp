﻿using Microsoft.AspNetCore.Mvc;

namespace APIRESTSQLserver.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PrincipalController : Controller
    {
        
        [HttpGet("ConsultarSQLServer")]
        public JsonResult Consultar ()
        {

            try
            {
                var Consulta = new StorageSQL();
                var Lista = Consulta.Consulta();
                var clientes = new ClientesResponse()
                {
                    status = true,
                    data = Lista,
                };

                //throw new System.Exception("Error de prueba");

                return Json(clientes);
            }
            catch (System.Exception ex)
            {
                var clientes = new ClientesResponse()
                {
                    status = false,
                    message = ex.Message
                };

                return Json(clientes);
            }
        }
        [HttpGet("AlmacenarSQLServer")]
        public string Almacenar(string Nombre, string Carrera, string Semestre, double Saldo)
        {
            var almacena = new StorageSQL();
            bool res = almacena.save("", "", "", 0, 0, "", "", "", "");
            if (res)
            {
                return "Guardado en SQL server desde .Net 5";
            }
            else
            {
                return "Erro al guardar";
            }

        }

    }
}
